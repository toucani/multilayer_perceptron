#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    common.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/07/08 15:06:35 by dkovalch          #+#    #+#              #
#    Updated: 2018/07/08 15:23:36 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of multilayer_perceptron project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import csv

import numpy


def is_malignant(value):
    return value == 1 or value == 'M'


def is_benign(value):
    return value == 0 or value == 'B'


def rescale_data(data):
    min_values = numpy.min(data, axis=0)
    max_values = numpy.max(data, axis=0)
    return (data - min_values) / (max_values - min_values)


def load_dataset(filename):
    csv_data = csv.reader(open(filename.strip()), delimiter=',')
    csv_data = [[line[col].strip() for col in range(len(line))] for line in csv_data]
    for line in csv_data:
        del line[0]  # Deleting ID of the patient
        # Converting diagnosis to a number
        line[0] = 1 if is_malignant(line[0]) else 0
    csv_data = numpy.array([[float(element) for element in line] for line in csv_data])

    # Splitting into diagnosis and the data itself
    data = csv_data.T
    diagnosis = numpy.array([data[0]])
    data = numpy.delete(data, 0, 0).T
    return diagnosis, rescale_data(data)
