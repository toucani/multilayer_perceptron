#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    predict.py                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/07/01 21:32:03 by dkovalch          #+#    #+#              #
#    Updated: 2018/07/08 15:23:36 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of multilayer_perceptron project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#


import sys

import numpy

from common import load_dataset
from network import NeuralNetwork


def predict(filename):
    nn = NeuralNetwork.load()
    print(repr(nn))
    dataset_result, dataset_data = load_dataset(filename)
    dataset_result = dataset_result.T
    prediction = nn.predict(dataset_data)
    result = dataset_result * numpy.log(prediction) + (1 - dataset_result) * numpy.log(1 - prediction)
    result = numpy.array(result).sum()
    result = - (result / len(prediction))
    print("Binary cross-entropy result: {}".format(result))


if __name__ == "__main__":
    try:
        filename = sys.argv[1] if len(sys.argv) > 1 else "data.csv"
        predict(filename)
    except IOError as err:
        print(err)
