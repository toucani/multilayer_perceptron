# multilayer perceptron


Multilayer perceptron a another machine learning project for 42 curriculum.
The goal is simple - build a forwardfeed neural network(with at least 2 hidden layers)
to predict cancer diagnosis using medical dataset(Wisconsin breast cancer).

## Installing

Just clone this repository.
To run the program you need python3 and pip.

## Using

Train the neural network
```
./train.py
```

Check how accurate it is:
```
./predict.py
```


## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=multilayer perceptron from Bitbucket)
* MitrikSicilian@icloud.com

## Many thanks

* to UNIT Factory, for inspiration to do our best.
* to all UNIT Factory students, who shared their knowledge and tested this project.

## UNIT Factory
![UNIT Factory logo](https://unit.ua/static/img/logo.png)

[UNIT Factory](https://uk.wikipedia.org/wiki/UNIT_Factory) was an innovative programming school and a part of [School 42](https://en.wikipedia.org/wiki/42_(school)) in [the heart](https://en.wikipedia.org/wiki/Kyiv) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).
