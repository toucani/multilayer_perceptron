#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    train.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/07/01 21:32:03 by dkovalch          #+#    #+#              #
#    Updated: 2018/07/08 15:23:36 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of multilayer_perceptron project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys

import matplotlib.pyplot as plt
import numpy

from common import load_dataset
from layer import NeuralNetworkLayer
from network import NeuralNetwork


def train(filename, verbose, graph):
    # Input layer is specified by a number of
    # inputs_per_neuron for the next level(30)
    neural_network = NeuralNetwork([
        NeuralNetworkLayer(20, 30),
        NeuralNetworkLayer(10, 20),
        NeuralNetworkLayer(5, 10),
        NeuralNetworkLayer(1, 5)])
    print(repr(neural_network))

    dataset_result, dataset_data = load_dataset(filename)
    dataset_result = dataset_result.T

    print("Training:")
    split = int(len(dataset_data) / 5)  # 20%
    metrics = neural_network.train(dataset_data[split:], dataset_result[split:],
                                   dataset_data[:split], dataset_result[:split],
                                   750, 1e-2, verbose)
    neural_network.save()

    if graph:
        plt.plot(metrics["iteration"], metrics["train"], 'go--', label="Training data")
        plt.plot(metrics["iteration"], metrics["validation"], 'ro--', label="Validation data")
        plt.ylabel("MSE")
        plt.xlabel("Iterations")
        plt.legend()
        plt.show()


if __name__ == "__main__":
    try:
        numpy.random.seed(7)
        filename = sys.argv[1] if len(sys.argv) > 1 else "data.csv"
        train(filename, True, True)
    except IOError as err:
        print(err)
