#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    network.py                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/07/01 12:26:33 by dkovalch          #+#    #+#              #
#    Updated: 2018/07/08 15:23:36 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
#   This file is part of multilayer_perceptron project.
#   Copyright (C) 2018, Dmytro Kovalchuk (dkovalch@student.unit.ua).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import pickle

import numpy


class NeuralNetwork:
    def __init__(self, layers):
        self.layers = layers

    def save(self, filename="results.pckl"):
        with open(filename, "wb") as file:
            pickle.dump(self, file)

    @classmethod
    def load(cls, filename="results.pckl"):
        with open(filename, "rb") as file:
            return pickle.load(file)

    def predict(self, values):
        out = self.__think(values)
        return out[-1]

    def train(self, train_in, train_out, validation_in, validation_out,
              max_iterations = 50000, learning_rate=1e-2, verbose=False):
        metrics = {"iteration": [], "train": [], "validation": []}
        for iteration in range(max_iterations):
            prediction = self.__think(train_in)
            # backward propagation
            layer_delta = self.__count_error(prediction, train_out)
            self.__ajust(train_in, prediction, layer_delta, learning_rate)
            # metrics
            if verbose and iteration % (max_iterations / 10) == 0:
                metrics["iteration"].append(iteration)
                self.__update_metrics(metrics, prediction, train_out, validation_in, validation_out)
                self.__print_metrics(metrics, validation_in is not None)
        return metrics

    # Per-layer error and delta correction measurement
    def __count_error(self, prediction, outputs):
        error = [[]] * len(self.layers)
        delta = [[]] * len(self.layers)

        # The last(first for us) layer
        error[-1] = outputs - prediction[-1]
        delta[-1] = error[-1] * self.__sigmoid_derivative(prediction[-1])

        for ct in reversed(range(len(self.layers) - 1)):
            error[ct] = numpy.dot(delta[ct + 1], self.layers[ct + 1].synaptic_weights.T)
            delta[ct] = error[ct] * self.__sigmoid_derivative(prediction[ct])
        return delta

    def __ajust(self, input, prediction, delta, learning_rate):
        self.layers[0].synaptic_weights += input.T.dot(delta[0]) * learning_rate
        for ct in range(1, len(self.layers)):
            self.layers[ct].synaptic_weights += prediction[ct - 1].T.dot(delta[ct]) * learning_rate

    # Forward propagation
    def __think(self, values):
        # Small hack to feed the inputs into the first layer
        result = [values]
        for layer in self.layers:
            result.append(self.__sigmoid(numpy.dot(result[-1], layer.synaptic_weights)))
        del result[0]
        return result

    def __update_metrics(self, metrics, prediction, train_out, validation_in, validation_out):
        mse_train = ((train_out - prediction[-1]) ** 2).mean()
        metrics["train"].append(mse_train)
        if validation_in is not None:
            validation_prediction = self.__think(validation_in)
            validation_mse = ((validation_out - validation_prediction[-1]) ** 2).mean()
            metrics["validation"].append(validation_mse)

    @staticmethod
    def __print_metrics(metrics, print_validation):
        print("Train mse: {:3.5}".format(metrics["train"][-1]))
        if print_validation:
            print("Validation mse: {:3.5}".format(metrics["validation"][-1]))

    @staticmethod
    def __sigmoid(x):
        return 1 / (1 + numpy.exp(-x))

    @staticmethod
    def __sigmoid_derivative(x):
        return x * (1 - x)

    def __repr__(self):
        return "Neural Network with {} layers, {} inputs and {} outputs.". \
            format(len(self.layers), self.layers[0].number_of_inputs_per_neuron, self.layers[-1].number_of_neurons)

    def __str__(self):
        layers_repr = ""
        for layer in self.layers:
            layers_repr += str(layer) + "\n"
        return repr(self) + "\n" + layers_repr
